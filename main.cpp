///////////////////////////////////////////////////////////////////////////////
//
// main: Handles the program entry and startup
// Copyright (C) EnterpriseDB, 2010-2012
// This software is released under the PostgreSQL Licence
// Dave Page <dave.page@enterprisedb.com>
//
///////////////////////////////////////////////////////////////////////////////

#include <QtGui/QApplication>
#include "pgtray.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Instantiate the tray icon, but don't show it
    // as it's not really a usable window.
    pgtray w;

    return a.exec();
}

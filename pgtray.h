///////////////////////////////////////////////////////////////////////////////
//
// pgtray class: Handles the tray icon and menu
// Copyright (C) EnterpriseDB, 2010-2012
// This software is released under the PostgreSQL Licence
// Dave Page <dave.page@enterprisedb.com>
//
///////////////////////////////////////////////////////////////////////////////

#ifndef PGTRAY_H
#define PGTRAY_H

#include <QtGui>

// Icons!
#ifdef Q_OS_MAC
#define ICON_TRAY QIcon(":tray-bw.png")
#else
#define ICON_TRAY QIcon(":tray.png")
#endif
#define ICON_RUNNING QIcon(":running.png")
#define ICON_STOPPED QIcon(":stopped.png")
#define ICON_TRANSITIONING QIcon(":transitioning.png")
#define ICON_START QIcon(":start.png")
#define ICON_STOP QIcon(":stop.png")
#define ICON_RESTART QIcon(":restart.png")
#define ICON_RELOAD QIcon(":reload.png")

class pgtray : public QWidget
{
    Q_OBJECT

public:
    pgtray();
    ~pgtray();

    void createActions();
    void createTrayIcon();

private:
    void AddMenuItems(QSettings *registry, QString majorVersion);
    void AddActionProperties(QSettings *registry, QString majorVersion, QAction *action);

    QAction *quitAction;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

private slots:
    void Quit();
    void Start();
    void Stop();
    void Restart();
    void Reload();
};

#endif // PGTRAY_H

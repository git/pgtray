///////////////////////////////////////////////////////////////////////////////
//
// pgtray class: Handles the tray icon and menu
// Copyright (C) EnterpriseDB, 2010-2012
// This software is released under the PostgreSQL Licence
// Dave Page <dave.page@enterprisedb.com>
//
///////////////////////////////////////////////////////////////////////////////

#include <QtGui>
#include <QApplication>

#include "pgtray.h"

//
// Constructor
//
pgtray::pgtray()
: QWidget(NULL)
{
    // Create the menus
    createActions();

    // Create the tray icon
    createTrayIcon();
}

//
// Destructor
//
pgtray::~pgtray()
{

}

//
// Create the menu actions
//
void pgtray::createActions()
{
    // Quit
    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), this, SLOT(Quit()));
}

//
// Create the tray icon
//
void pgtray::createTrayIcon()
{
    // Check we can find the system tray
    if (!QSystemTrayIcon::isSystemTrayAvailable())
    {
        QMessageBox::critical(0, tr("PG Tray Error"), tr("The system tray could not be found on this system. Closing pgtray."));
        QCoreApplication::exit(-1);
    }

    //
    // Build the menu
    // Most of the menu is built from the list of installed servers.
    //
    trayIconMenu = new QMenu(this);

    // QSettings (4.8) can't enumerate sub-groups in an INI file if they are part of the section
    // header (e.g. [PostgreSQL/9.1]. This is the case with the postgres-reg.ini file. Therefore
    // we'll use a regexp to get all the matching section headers, and work it all out from there.
    QStringList majorVersions;
    int pos = 0;

    QFile registryFile("/etc/postgres-reg.ini");
    if (!registryFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::critical(this, tr("PG Tray Error"), "Could not read the registry file /etc/postgres-reg.ini");
        QCoreApplication::exit(-1);
    }
    QTextStream registryStream(&registryFile);
    QString registryData = registryStream.readAll();
    registryFile.close();

    // PostgreSQL Servers
    QRegExp registryRegexp("\\[(PostgreSQL/[0-9]+\\.[0-9]+)\\]");
    registryRegexp.indexIn(registryData);

    while ((pos = registryRegexp.indexIn(registryData, pos)) != -1)
    {
         majorVersions << registryRegexp.cap(1);
         pos += registryRegexp.matchedLength();
     }

    QSettings *registry = new QSettings("/etc/postgres-reg.ini", QSettings::IniFormat);

    foreach (const QString &majorVersion, majorVersions)
        AddMenuItems(registry, majorVersion);

    // Postgres Plus Advanced Server
    majorVersions.clear();
    pos = 0;

    registryRegexp.setPattern("\\[(EnterpriseDB/[0-9]+\\.[0-9]+AS)\\]");
    registryRegexp.indexIn(registryData);

    while ((pos = registryRegexp.indexIn(registryData, pos)) != -1)
    {
        if (!majorVersions.count() && !trayIconMenu->isEmpty())
            trayIconMenu->addSeparator();

         majorVersions << registryRegexp.cap(1);
         pos += registryRegexp.matchedLength();
     }

    foreach (const QString &majorVersion, majorVersions)
        AddMenuItems(registry, majorVersion);

    // Quit
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    // Create the tray icon
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);

    // Setup the icon itself. For convenience, we'll also use it for the dialogue.
    trayIcon->setIcon(ICON_TRAY);
    setWindowIcon(ICON_TRAY);
    trayIcon->setToolTip(tr("PostgreSQL tray monitor"));

    trayIcon->show();
}

//
// Add menu items for a server
//
void pgtray::AddMenuItems(QSettings *registry, QString majorVersion)
{
    QMenu *menu = trayIconMenu->addMenu(ICON_STOPPED, registry->value(majorVersion + tr("/Description")).toString());

    // Start
    QAction *action = new QAction(ICON_START, tr("&Start server"), this);
    connect(action, SIGNAL(triggered()), this, SLOT(Start()));
    AddActionProperties(registry, majorVersion, action);
    menu->addAction(action);

    // Stop
    action = new QAction(ICON_STOP, tr("S&top server"), this);
    connect(action, SIGNAL(triggered()), this, SLOT(Stop()));
    AddActionProperties(registry, majorVersion, action);
    menu->addAction(action);

    // Restart
    action = new QAction(ICON_RESTART, tr("&Restart server"), this);
    connect(action, SIGNAL(triggered()), this, SLOT(Restart()));
    AddActionProperties(registry, majorVersion, action);
    menu->addAction(action);

    // Restart
    action = new QAction(ICON_RELOAD, tr("R&eload configuration"), this);
    connect(action, SIGNAL(triggered()), this, SLOT(Reload()));
    AddActionProperties(registry, majorVersion, action);
    menu->addAction(action);
}

//
// Add menu item properties for a server
//
void pgtray::AddActionProperties(QSettings *registry, QString majorVersion, QAction *action)
{
    action->setProperty("Description", registry->value(majorVersion + tr("/Description")).toString());
    action->setProperty("Version", registry->value(majorVersion + tr("/Version")).toString());
    action->setProperty("InstallationDirectory", registry->value(majorVersion + tr("/InstallationDirectory")).toString());
    action->setProperty("DataDirectory", registry->value(majorVersion + tr("/DataDirectory")).toString());
    action->setProperty("Locale", registry->value(majorVersion + tr("/Locale")).toString());
    action->setProperty("Port", registry->value(majorVersion + tr("/Port")).toString());
    action->setProperty("ServiceAccount", registry->value(majorVersion + tr("/ServiceAccount")).toString());
    action->setProperty("Superuser", registry->value(majorVersion + tr("/Superuser")).toString());
    action->setProperty("ServiceID", registry->value(majorVersion + tr("/ServiceID")).toString());
}

//
// Process the Quit slot
//
void pgtray::Quit()
{
    // Kill the icon
    if (trayIcon)
        trayIcon->hide();

    // Kill the app
    QCoreApplication::exit(-1);
}

//
// Process the Start slot
//
void pgtray::Start()
{
    QAction *action = (QAction *)QObject::sender();

    QMessageBox::information(this, tr("PG Tray"), tr("Starting ") + action->property("Description").toString());
}

//
// Process the Stop slot
//
void pgtray::Stop()
{

}

//
// Process the Restart slot
//
void pgtray::Restart()
{

}

//
// Process the Reload slot
//
void pgtray::Reload()
{

}

##############################################################################
#
# pgtray: Qt Creator project file
# Copyright (C) EnterpriseDB, 2010-2012
# This software is released under the PostgreSQL Licence
# Dave Page <dave.page@enterprisedb.com>
#
##############################################################################

QT       += core gui

TARGET = pgtray
TEMPLATE = app


SOURCES += main.cpp\
        pgtray.cpp

HEADERS  += pgtray.h

FORMS    +=

RESOURCES += \
    pgtray.qrc

RC_FILE += \
    pgtray.rc

# We need a custom Info.plist file on Mac to hide from the Dock
QMAKE_INFO_PLIST = Info.plist

OTHER_FILES += \
    Info.plist
